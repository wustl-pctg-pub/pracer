# Makefile

include config.mak
include ../../Makefile.myenv

.PHONY: all serial cilk piper cilkview default fprofiled \
clean-serial clean-cilk clean-piper clean-cilkview clean distclean \
install uninstall dox test testclean run

all: 
	@echo "which one? pick serial, cilk, piper or cilkview"

SRCS = common/mc.c common/predict.c common/pixel.c common/macroblock.c \
       common/frame.c common/dct.c common/cpu.c common/cabac.c \
       common/common.c common/mdate.c common/set.c \
       common/quant.c common/vlc.c \
       encoder/analyse.c encoder/me.c encoder/ratecontrol.c \
       encoder/set.c encoder/macroblock.c encoder/cabac.c \
       encoder/cavlc.c ktiming.c

SRCCLI := matroska.c muxers.c

COMPILER_HOME=WORK_DIR/llvm-cilk
#PIPER_HOME=/project/adams/home/xuyifan/2D/piper_rts
PIPER_HOME=WORK_DIR/piper-rts
RD_HOME=WORK_DIR/rdtool
RD_INCLUDE=$(RD_HOME)/include 
RTS_INCLUDE=$(PIPER_HOME)/include
RTS_STATIC_LIB=$(PIPER_HOME)/lib/libcilkrts.a
RTS_DYNAMIC_LIB=-Wl,-rpath -Wl,$(PIPER_HOME)/lib/libcilkrts.so -lcilkrts

PIPER_CC=$(COMPILER_HOME)/bin/clang
PIPER_CXX=$(COMPILER_HOME)/bin/clang++

PIPER_LDLIBS=-lpthread -ldl -lrt  -lm -lstdc++


# compilation flags
PIPER_CC=$(COMPILER_HOME)/bin/clang
PIPER_CXX=$(COMPILER_HOME)/bin/clang++

VPATH = $(RD_HOME)/src:$(RD_HOME)/src/om


# CILK_STUFF
RTS_LIBPATH = $(CILKM_RTS)/runtime/.libs
CILK_FLAGS = -DCILK=1 -I$(CILKM_RTS) -I$(CILKM_RTS)/runtime 
CILK_LDFLAGS = -L$(CILKM_RTS)/runtime -L$(RTS_LIBPATH) \
-lcilkrt0 -lcilk -Wl,-rpath,$(RTS_LIBPATH)

# JSUKHA: For Piper, I am defining CILK because I'm too lazy to figure
# out the differences for now.
RD_FLAGS = -DPIPER=1 -g -O3 -fcilkplus -fno-omit-frame-pointer -std=c++11 -D__CILKRTS_ABI_VERSION=1 -fcilk-no-inline -fno-vectorize -fno-slp-vectorize 
PIPER_FLAGS = -DPIPER=1 -g -O3 -fcilkplus -fno-omit-frame-pointer -std=c++11 -D__CILKRTS_ABI_VERSION=1 -fcilk-no-inline -fno-vectorize -fno-slp-vectorize
PIPER_CFLAGS = -DPIPER=1 -g -O3 -fcilkplus -fno-omit-frame-pointer -D__CILKRTS_ABI_VERSION=1 -fcilk-no-inline -fno-vectorize -fno-slp-vectorize
#PIPER_LDFLAGS = -L$(CILKPLUS_PIPER_LIB)/lib
PIPER_LDFLAGS = -L$(CILKPLUS_PIPER_LIB)/lib -Wl,-rpath,$(CILKPLUS_PIPER_LIB)/lib

# directories to dump the compiled object file and library
CILK_OBJDIR = build/cilk
SERIAL_OBJDIR = build/serial
PIPER_OBJDIR = build/piper
CILKVIEW_OBJDIR = build/cilkview

# Visualization sources
ifeq ($(VIS),yes)
SRCS   += common/visualize.c common/display-x11.c
endif

ifndef ($(RANLIB))
RANLIB = /usr/bin/ranlib
endif

AS=/project/adams/home/xuyifan/2D/yasm/bin/yasm

# MMX/SSE optims
ifneq ($(AS),)
X86SRC0 = cabac-a.asm dct-a.asm deblock-a.asm mc-a.asm mc-a2.asm \
          pixel-a.asm predict-a.asm quant-a.asm sad-a.asm \
          cpu-a.asm dct-32.asm
X86SRC = $(X86SRC0:%=common/x86/%)

ifeq ($(ARCH),X86)
ARCH_X86 = yes
ASMSRC   = $(X86SRC) common/x86/pixel-32.asm
endif

ifeq ($(ARCH),X86_64)
ARCH_X86 = yes
ASMSRC   = $(X86SRC:-32.asm=-64.asm)
ASFLAGS += -DARCH_X86_64 -g dwarf2
endif

ifdef ARCH_X86
ASFLAGS += -Icommon/x86/
SRCS   += common/x86/mc-c.c common/x86/predict-c.c
OBJASM  = $(ASMSRC:%.asm=%.o)
$(OBJASM): common/x86/x86inc.asm common/x86/x86util.asm
checkasm: tools/checkasm-a.o
endif
endif

# AltiVec optims
ifeq ($(ARCH),PPC)
ALTIVECSRC += common/ppc/mc.c common/ppc/pixel.c common/ppc/dct.c \
              common/ppc/quant.c common/ppc/deblock.c \
              common/ppc/predict.c
SRCS += $(ALTIVECSRC)
$(ALTIVECSRC:%.c=%.o): CFLAGS += $(ALTIVECFLAGS)
endif

# VIS optims
ifeq ($(ARCH),UltraSparc)
ASMSRC += common/sparc/pixel.asm
OBJASM  = $(ASMSRC:%.asm=%.o)
endif

ifneq ($(HAVE_GETOPT_LONG),1)
SRCS += extras/getopt.c
endif

SERIAL_OBJS = $(SRCS:%.c=$(SERIAL_OBJDIR)/%.o)
SERIAL_OBJCLI = $(SRCCLI:%.c=$(SERIAL_OBJDIR)/%.o)
SERIAL_OBJASM = $(OBJASM:%.o=$(SERIAL_OBJDIR)/%.o)

CILK_OBJS = $(SRCS:%.c=$(CILK_OBJDIR)/%.o)
CILK_OBJCLI = $(SRCCLI:%.c=$(CILK_OBJDIR)/%.o)
CILK_OBJASM = $(OBJASM:%.o=$(CILK_OBJDIR)/%.o)

PIPER_OBJS = $(SRCS:%.c=$(PIPER_OBJDIR)/%.o)
PIPER_OBJCLI = $(SRCCLI:%.c=$(PIPER_OBJDIR)/%.o)
PIPER_OBJASM = $(OBJASM:%.o=$(PIPER_OBJDIR)/%.o)

CFLAGS=-I.

serial_dirs:
	mkdir -p $(SERIAL_OBJDIR)
	mkdir -p $(SERIAL_OBJDIR)/common
	mkdir -p $(SERIAL_OBJDIR)/common/x86
	mkdir -p $(SERIAL_OBJDIR)/common/ppc
	mkdir -p $(SERIAL_OBJDIR)/common/sparc
	mkdir -p $(SERIAL_OBJDIR)/encoder
	mkdir -p $(SERIAL_OBJDIR)/extra
	mkdir -p $(SERIAL_OBJDIR)/tools

cilk_dirs:
	mkdir -p $(CILK_OBJDIR)
	mkdir -p $(CILK_OBJDIR)/common
	mkdir -p $(CILK_OBJDIR)/common/x86
	mkdir -p $(CILK_OBJDIR)/common/ppc
	mkdir -p $(CILK_OBJDIR)/common/sparc
	mkdir -p $(CILK_OBJDIR)/encoder
	mkdir -p $(CILK_OBJDIR)/extra
	mkdir -p $(CILK_OBJDIR)/tools

piper_dirs:
	mkdir -p $(PIPER_OBJDIR)
	mkdir -p $(PIPER_OBJDIR)/common
	mkdir -p $(PIPER_OBJDIR)/common/x86
	mkdir -p $(PIPER_OBJDIR)/common/ppc
	mkdir -p $(PIPER_OBJDIR)/common/sparc
	mkdir -p $(PIPER_OBJDIR)/encoder
	mkdir -p $(PIPER_OBJDIR)/extra
	mkdir -p $(PIPER_OBJDIR)/tools

cilkview_dirs:
	mkdir -p $(CILKVIEW_OBJDIR)

$(SERIAL_OBJDIR)/%.o: %.c
	@echo "$(CC) $(CFLAGS) -c '$<' -o '$@'"
	$(CC) $(CFLAGS) -c $< -o $@

$(CILK_OBJDIR)/%.o: %.c
	@echo "$(CC) $(CFLAGS) $(CILK_FLAGS) -c '$<' -o '$@'"
	$(CC) $(CFLAGS) $(CILK_FLAGS) -c $< -o $@

$(PIPER_OBJDIR)/%.o: %.c
	@echo "icc $(CFLAGS) $(PIPER_FLAGS) -std=c++0x -c '$<' -o '$@'"
	$(PIPER_CC) $(CFLAGS) $(PIPER_CFLAGS) -c $< -o $@

$(CILK_OBJDIR)/encoder/encoder-cilk.o: encoder/encoder-cilk.c encoder/encoder-stages.c 
	@echo "$(CC) $(CFLAGS) $(CILK_FLAGS) -c '$<' -o '$@'"
	$(CC) $(CFLAGS) $(CILK_FLAGS) -c $< -o $@

# $(PIPER_OBJDIR)/%.o: %.c
# 	@echo "icc $(CFLAGS) -DPIPER=1 -c '$<' -o '$@'"
# 	icc $(CFLAGS) -DPIPER=1 -c $< -o $@

# ANGE: a special rule for encoder-piper.cpp since it needs -std=c++0x 
#       to link with Cilk Plus piper runtime header file, which is in C++.
$(PIPER_OBJDIR)/encoder/encoder-piper.o: encoder/encoder-piper.cpp
	@echo "icc $(CFLAGS) $(PIPER_FLAGS) -std=c++0x -c '$<' -o '$@'"
	#icc $(CFLAGS) $(PIPER_FLAGS) -std=c++0x -c $< -o $@
	$(PIPER_CXX) -I. $(PIPER_FLAGS) -I$(RTS_INCLUDE) -I$(RD_HOME) -I$(RD_INCLUDE) -c $< -o $@

# ANGE: a special rule for encoder-cilkview.c 
$(PIPER_OBJDIR)/encoder/encoder-cilkview.o: encoder/encoder-cilkview.c
	@echo "icc $(CFLAGS) -DPIPER=1 -c '$<' -o '$@'"
	icc $(CFLAGS) -DPIPER=1 -c $< -o $@

$(SERIAL_OBJDIR)/libx264.a: $(SERIAL_OBJS) $(SERIAL_OBJASM)
	$(AR) rc $(SERIAL_OBJDIR)/libx264.a $(SERIAL_OBJS) $(SERIAL_OBJASM)
	$(RANLIB) $(SERIAL_OBJDIR)/libx264.a

$(CILK_OBJDIR)/libx264.a: $(CILK_OBJS) $(CILK_OBJASM)
	$(AR) rc $(CILK_OBJDIR)/libx264.a $(CILK_OBJS) $(CILK_OBJASM)
	$(RANLIB) $(CILK_OBJDIR)/libx264.a

$(PIPER_OBJDIR)/libx264.a: $(PIPER_OBJS) $(PIPER_OBJASM)
	$(AR) rc $(PIPER_OBJDIR)/libx264.a $(PIPER_OBJS) $(PIPER_OBJASM)
	$(RANLIB) $(PIPER_OBJDIR)/libx264.a

serial: serial_dirs $(SERIAL_OBJDIR)/x264-serial$(EXE)

cilk: cilk_dirs $(CILK_OBJDIR)/x264-cilk$(EXE)

piper: piper_dirs $(PIPER_OBJDIR)/x264-piper$(EXE)

# We share dir w/ piper build for cilkview build, since they both use icc
cilkview: cilkview_dirs piper_dirs $(CILKVIEW_OBJDIR)/x264-cilkview$(EXE)


# ANGE: not used
# $(SONAME): $(OBJS) $(OBJASM)
#	$(CC) -shared -o $@ $(OBJS) $(OBJASM) $(SOFLAGS) $(LDFLAGS)

$(SERIAL_OBJDIR)/x264-serial$(EXE): $(SERIAL_OBJDIR)/encoder/encoder.o \
$(SERIAL_OBJDIR)/x264-serial.o $(SERIAL_OBJCLI) $(SERIAL_OBJDIR)/libx264.a
	$(CC) -o $@ $+ $(LDFLAGS)

$(CILK_OBJDIR)/x264-cilk$(EXE): $(CILK_OBJDIR)/encoder/encoder-cilk.o \
$(CILK_OBJDIR)/x264-cilk.o $(CILK_OBJCLI) $(CILK_OBJDIR)/libx264.a
	$(CC) -o $@ $+ $(LDFLAGS) $(CILK_LDFLAGS)

$(PIPER_OBJDIR)/x264-piper$(EXE): $(PIPER_OBJDIR)/encoder/encoder-piper.o \
$(PIPER_OBJDIR)/x264-piper.o $(PIPER_OBJCLI) $(PIPER_OBJDIR)/libx264.a 
	#icc -o $@ $+ $(LDFLAGS) $(PIPER_LDFLAGS)
	$(PIPER_CXX)   $^ $(RTS_STATIC_LIB) $(PIPER_LDLIBS) $(LDFLAGS)  -o $@ 

%.o: %.cpp
	$(PIPER_CXX) $(RD_FLAGS) -I$(RTS_INCLUDE) -I$(RD_HOME) -I$(RD_INCLUDE) -IWORK_DIR/batched-piper/runtime -c $< -o $@

$(PIPER_OBJDIR)/x264-cilkview$(EXE): $(PIPER_OBJDIR)/encoder/encoder-cilkview.o \
$(PIPER_OBJDIR)/x264-cilkview.o $(PIPER_OBJCLI) $(PIPER_OBJDIR)/libx264.a
	icc -o $@ $+ $(LDFLAGS)

$(CILKVIEW_OBJDIR)/x264-cilkview$(EXE): $(PIPER_OBJDIR)/x264-cilkview$(EXE)
	cd $(CILKVIEW_OBJDIR)
	ln -sf ../piper/x264-cilkview $(CILKVIEW_OBJDIR)/

checkasm: $(SERIAL_OBJDIR)/tools/checkasm.o $(SERIAL_OBJDIR)/libx264.a
	$(CC) -o $@ $+ $(LDFLAGS)

$(SERIAL_OBJDIR)/%.o: %.asm
	$(AS) $(ASFLAGS) -o $@ $<
# ANGE: This didn't work
# delete local/anonymous symbols, so they don't show up in oprofile
#	-@ $(STRIP) -x $@

$(CILK_OBJDIR)/%.o: %.asm
	$(AS) $(ASFLAGS) -o $@ $<

$(PIPER_OBJDIR)/%.o: %.asm
	$(AS) $(ASFLAGS) -o $@ $<

config.mak: $(wildcard .svn/entries */.svn/entries */*/.svn/entries)
#	./configure $(CONFIGURE_ARGS)


SRC2 = $(SRCS) $(SRCCLI)
# These should cover most of the important codepaths
OPT0 = --crf 30 -b1 -m1 -r1 --me dia --no-cabac --pre-scenecut --direct temporal --no-ssim --no-psnr
OPT1 = --crf 16 -b2 -m3 -r3 --me hex -8 --direct spatial --no-dct-decimate
OPT2 = --crf 26 -b2 -m5 -r2 --me hex -8 -w --cqm jvt --nr 100
OPT3 = --crf 18 -b3 -m9 -r5 --me umh -8 -t1 -A all --mixed-refs -w --b-pyramid --direct auto --no-fast-pskip
OPT4 = --crf 22 -b3 -m7 -r4 --me esa -8 -t2 -A all --mixed-refs
OPT5 = --frames 50 --crf 24 -b3 -m9 -r3 --me tesa -8 -t1 --mixed-refs
OPT6 = --frames 50 -q0 -m9 -r2 --me hex -Aall
OPT7 = --frames 50 -q0 -m2 -r1 --me hex --no-cabac

ifeq (,$(VIDS))
fprofiled:
	@echo 'usage: make fprofiled VIDS="infile1 infile2 ..."'
	@echo 'where infiles are anything that x264 understands,'
	@echo 'i.e. YUV with resolution in the filename, y4m, or avisynth.'
else
fprofiled:
	$(MAKE) clean
	mv config.mak config.mak2
	sed -e 's/CFLAGS.*/& -fprofile-generate/; s/LDFLAGS.*/& -fprofile-generate/' config.mak2 > config.mak
	$(MAKE) x264$(EXE)
	$(foreach V, $(VIDS), $(foreach I, 0 1 2 3 4 5 6 7, ./x264$(EXE) $(OPT$I) $(V) --progress -o $(DEVNULL) ;))
	rm -f $(SRC2:%.c=%.o)
	sed -e 's/CFLAGS.*/& -fprofile-use/; s/LDFLAGS.*/& -fprofile-use/' config.mak2 > config.mak
	$(MAKE)
	rm -f $(SRC2:%.c=%.gcda) $(SRC2:%.c=%.gcno)
	mv config.mak2 config.mak
endif


clean-serial:
	rm -f $(SERIAL_OBJDIR)/encoder/encoder.o $(SERIAL_OBJDIR)/x264-serial.o
	rm -f $(SERIAL_OBJS) $(SERIAL_OBJASM) $(SERIAL_OBJCLI)
	rm -f $(SERIAL_OBJDIR)/*.a $(SERIAL_OBJDIR)/x264-serial$(EXE) 
	rm -f checkasm checkasm.exe $(SERIAL_OBJDIR)/tools/checkasm.o
	rm -f $(SRC2:%.c=%.gcda) $(SRC2:%.c=%.gcno)
	- sed -e 's/ *-fprofile-\(generate\|use\)//g' config.mak > config.mak2 && mv config.mak2 config.mak

clean-cilk:
	rm -f $(CILK_OBJDIR)/encoder/encoder-cilk.o $(CILK_OBJDIR)/x264-cilk.o
	rm -f $(CILK_OBJS) $(CILK_OBJASM) $(CILK_OBJCLI)
	rm -f $(CILK_OBJDIR)/*.a $(CILK_OBJDIR)/x264-cilk$(EXE)

clean-piper:
	rm -f $(PIPER_OBJDIR)/encoder/encoder-piper.o $(PIPER_OBJDIR)/x264-piper.o
	rm -f $(PIPER_OBJS) $(PIPER_OBJASM) $(PIPER_OBJCLI)
	rm -f $(PIPER_OBJDIR)/*.a $(PIPER_OBJDIR)/x264-piper$(EXE)

clean-cilkview:
	rm -f $(PIPER_OBJDIR)/encoder/encoder-cilkview.o
	rm -f $(PIPER_OBJDIR)/x264-cilkview.o
	rm -f $(PIPER_OBJDIR)/x264-cilkview$(EXE)
	rm -f $(CILKVIEW_OBJDIR)/x264-cilkview$(EXE)

clean: clean-serial clean-cilk clean-piper clean-cilkview

distclean: clean
	rm -f config.mak config.h x264.pc
	rm -rf test/

install: x264$(EXE) $(SONAME)
	install -d $(DESTDIR)$(bindir) $(DESTDIR)$(includedir)
	install -d $(DESTDIR)$(libdir) $(DESTDIR)$(libdir)/pkgconfig
	install -m 644 x264.h $(DESTDIR)$(includedir)
	install -m 644 libx264.a $(DESTDIR)$(libdir)
	install -m 644 x264.pc $(DESTDIR)$(libdir)/pkgconfig
	install x264$(EXE) $(DESTDIR)$(bindir)
	$(RANLIB) $(DESTDIR)$(libdir)/libx264.a
ifeq ($(SYS),MINGW)
	$(if $(SONAME), install -m 755 $(SONAME) $(DESTDIR)$(bindir))
else
	$(if $(SONAME), ln -sf $(SONAME) $(DESTDIR)$(libdir)/libx264.$(SOSUFFIX))
	$(if $(SONAME), install -m 755 $(SONAME) $(DESTDIR)$(libdir))
endif
	$(if $(IMPLIBNAME), install -m 644 $(IMPLIBNAME) $(DESTDIR)$(libdir))

uninstall:
	rm -f $(DESTDIR)$(includedir)/x264.h $(DESTDIR)$(libdir)/libx264.a
	rm -f $(DESTDIR)$(bindir)/x264 $(DESTDIR)$(libdir)/pkgconfig/x264.pc
	$(if $(SONAME), rm -f $(DESTDIR)$(libdir)/$(SONAME) $(DESTDIR)$(libdir)/libx264.$(SOSUFFIX))

etags: TAGS

TAGS:
	etags $(SRCS)

dox:
	doxygen Doxyfile

ifeq (,$(VIDS))
test:
	@echo 'usage: make test VIDS="infile1 infile2 ..."'
	@echo 'where infiles are anything that x264 understands,'
	@echo 'i.e. YUV with resolution in the filename, y4m, or avisynth.'
else
test:
	perl tools/regression-test.pl --version=head,current --options='$(OPT0)' --options='$(OPT1)' --options='$(OPT2)' $(VIDS:%=--input=%)
endif

testclean:
	rm -f test/*.log test/*.264
	$(foreach DIR, $(wildcard test/x264-r*/), cd $(DIR) ; make clean ; cd ../.. ;)
