if [ $# -lt 1 ]; then
    echo "Usage: ./play.sh <encoded file>"
fi
ffplay -f h264 $1
