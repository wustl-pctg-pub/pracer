/**
 * Copyright (c) 2012 MIT License by 6.172 Staff
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * Authors: Yifan Xu <xuyifan@wustl.edu>
 *          I-Ting Angelina Lee <angelee@wustl.edu>
 *          Jim Sukha <jim.sukha@intel.com>
**/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <cilk/piper.h>
#include "ktiming.h"
//#include "mem_access.h"
#include "piper_rd.h"
#include <cilk/cilk_api.h>
#include "instrumentation.h"

struct Out {
    int i;
    int j;
    char k;
};

void match(char * dict, char * buff, struct Out * o, int bs, int ds)
{
    //disable_checking();
    int i=0, j=0;
    //fprintf(stderr, "i address: %zu\n", ADDR_TO_KEY((long)(&i)));
    //fprintf(stderr, "j address: %zu\n", ADDR_TO_KEY((long)(&j)));
    int ti,tq,tj;
    //fprintf(stderr, "ti address: %zu\n", ADDR_TO_KEY((long)(&ti)));
    //fprintf(stderr, "tq address: %zu\n", ADDR_TO_KEY((long)(&tq)));
    //fprintf(stderr, "tj address: %zu\n", ADDR_TO_KEY((long)(&tj)));
    for (int q=0;q<ds;q++)
    {
        ti = q;
        tq = q;
        tj = 0;
        for(int p=0;p<bs-1;p++)
        {
            if (dict[tq] == buff[p])
            {
                tj++;
                tq = (tq == ds-1) ? q : tq+1;
            }
            else break;
        }
        
        if (tj > j)
        {
            j = tj;
            i = ti;
        }
    }
    o->i = i;
    o->j = j;
    o->k = buff[j];
    //enable_checking();
}

void shift( char* b, int len, int s )
{
    //disable_checking();
    for(int i=0;i<len-s;i++)
        b[i] = b[s+i];
    //enable_checking();
}

void writeIntermediate(int i, int j, int k, uint32_t *out) {
    //disable_checking();
    //fprintf(stderr, "i address: %zu\n", ADDR_TO_KEY((long)(&i)));
    //fprintf(stderr, "j address: %zu\n", ADDR_TO_KEY((long)(&j)));
    //fprintf(stderr, "k address: %zu\n", ADDR_TO_KEY((long)(&k)));
    //fprintf(stderr, "*out address: %zu\n", ADDR_TO_KEY((long)(&out)));
    *out = 0;
    *out = *out | (i << (6+8)) | (j << 8) | (int) k;
    //enable_checking();
    //fprintf(stderr, "address: %ld\n", (long)out);
    //fprintf(stderr, "out: %d\n", *out);
}

void compress(char *chunk, int count, int cs, int bs, int ds, uint32_t **outBuff, int &length) {
    for (int i = count; i < cs + 1; i++) 
        chunk[i] = '\0';
    
    *outBuff = (uint32_t *)malloc(count * sizeof(uint32_t));
    length = 0;
    char *dict = (char *)malloc(ds * sizeof(char));
    char *buff = (char *)malloc(bs * sizeof(char));

    int worker = __cilkrts_get_worker_number();
    //fprintf(stderr, "dict address: %zu iter: %lld worker: %d\n", ADDR_TO_KEY((long)(&dict)), g_curIterNum, worker);
    //fprintf(stderr, "iter: %lld\n", g_curIterNum);
    //fprintf(stderr, "buff address: %zu\n", ADDR_TO_KEY((long)(&buff)));

    for (int i = 0; i < bs; i++) 
        buff[i] = chunk[i];
    
    for (int i = 0; i < ds; i++) 
        dict[i] = '\0';
    
    chunk += bs;

    Out out;
    //fprintf(stderr, "out.i address: %zu\n", ADDR_TO_KEY((long)(&out.i)));
    //fprintf(stderr, "out.j address: %zu\n", ADDR_TO_KEY((long)(&out.j)));
    //fprintf(stderr, "out.k address: %zu\n", ADDR_TO_KEY((long)(&out.k)));
    //fprintf(stderr, "length: %zu\n", ADDR_TO_KEY((long)(&length)));
    //fprintf(stderr, "outBuff: %zu\n", ADDR_TO_KEY((long)(&outBuff)));
    //fprintf(stderr, "*outBuff: %zu\n", ADDR_TO_KEY((long)(*outBuff)));
    while (buff[0] != '\0') {
        match(dict, buff, &out, bs, ds);
        //writeIntermediate(out.i, out.j, out.k, *outBuff + length);//write to intermediate result
        //disable_checking();
        *(*outBuff + length) = 0;
        *(*outBuff + length) = *(*outBuff + length) | (out.i << (6+8)) | (out.j << 8) | (int) out.k;
        //fprintf(stderr, "*outBuff: %zu\n", ADDR_TO_KEY((long)(*outBuff + length)));
        //enable_checking();
        length++;

        //printf("(%d,%d,%c)\n",out.i,out.j,out.k);

        if (out.k == '\0')
            break;

        shift(dict, ds, out.j + 1);
        for (int i = 0; i < out.j + 1; i++)
            dict[ds - (out.j + 1) + i] = buff[i];

        shift(buff, bs, out.j + 1);
        for (int i = 0; i < out.j + 1; i++) {
            buff[bs - (out.j + 1) + i] = chunk[i];
            if (chunk[i] == '\0') {
                chunk = &chunk[i];
                break;
            }
        }

        if (chunk[0] != '\0')
            chunk += out.j + 1;
    }

    //for (int i = 0; i < length; i++) {
    //    fprintf(stderr, "outbuff: %d\n", *(*outBuff + i));
    //}

    free(dict);
    free(buff);
}

void write(FILE *fd, uint32_t *outBuff, int length) {
    for (int i = 0; i < length; i++) {
        fwrite(outBuff + i, 1, 4, fd);
//        fprintf(stderr, "out: %d\n", *(outBuff + length));
    }

    uint32_t out;
    writeIntermediate(-1, 0, ' ', &out);
//    fprintf(stderr, "out: %d\n", out);
    fwrite(&out, 1, 4, fd);
}

int main(int argc, char* argv[]) {
    if (argc < 7) {
        printf("Usage : %s <comp/dec> <dictionary size> <buffer size> <chunk size> infile outfile\n", argv[0]);
        return 0;
    }

    int comp = atoi(argv[1]);
    int ds = atoi(argv[2]);
    int bs = atoi(argv[3]);
    int cs = atoi(argv[4]);
    char *infile = argv[5];
    char *outfile = argv[6];

    FILE *infd = fopen(infile, "r");
    FILE *outfd = fopen(outfile, "w");
    
    clockmark_t begin, end;
    begin = ktiming_getmark();
    //compress
    CILK_PIPE_WHILE_BEGIN(!feof(infd)) {
        //stage 1: read
        char *chunk = (char *)malloc((cs + 1) * sizeof(char));
        //fprintf(stderr, "chunk address: %zu\n", ADDR_TO_KEY((long)(&chunk)));
        int count = fread(chunk, 1, cs, infd);
        //fprintf(stderr, "count address: %zu\n", ADDR_TO_KEY((long)(&count)));
        
        CILK_STAGE(1);
        //stage 2: compress
        //int worker = __cilkrts_get_worker_number();
        //fprintf(stderr, "outBuff     iter: %lld worker: %d\n", g_curIterNum, worker);
        uint32_t *outBuff = NULL;
        //fprintf(stderr, "outBuff address: %zu\n", ADDR_TO_KEY((long)(&outBuff)));
        int length = 0;
        //fprintf(stderr, "length address: %zu\n", ADDR_TO_KEY((long)(&length)));
        //outBuff = (uint32_t *)malloc(1 * sizeof(uint32_t));
        //length++;
        compress(chunk, count, cs, bs, ds, &outBuff, length);

        CILK_STAGE_WAIT(2);
        //stage 3: write
        write(outfd, outBuff, length);
        free(chunk);
        free(outBuff);
    } CILK_PIPE_WHILE_END();

    end = ktiming_getmark();
    double elapsed_time = ktiming_diff_sec(&begin, &end);
    printf("Elapsed time in second: %f\n", elapsed_time);

    fclose(infd);
    fclose(outfd);
} 
